---
title: CentOS Stream Kernel Documentation
---

<!-- scope -->
Welcome to documentation about development of Red Hat-based Linux kernel projects

: [CentOS stream 9 kernel](https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9)

: [Fedora/ARK kernel](https://gitlab.com/cki-project/kernel-ark)

: [Private RHEL kernels](https://gitlab.com/redhat/rhel/src/kernel)


<!-- prerequisites -->
It is expected that readers are familiar with

: [Git](https://git-scm.com/book/en/v2)

: [GitLab](https://docs.gitlab.com/ee/user/project/repository/)

: [Linux kernel development](https://docs.kernel.org/#working-with-the-development-community)

: [Backporting code changes from upstream trees](https://www.redhat.com/en/blog/what-backporting-and-how-does-it-apply-rhel-and-other-red-hat-products)


Main documents

: [Quick start guide](docs/readme.html)

: [RHEL kernel workflow](docs/rhel_kernel_workflow.html)
